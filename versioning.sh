#!/bin/sh
#VERSION=1.0.0
#VERSION_TYPE="patch"
ACT_VERSION=`git for-each-ref --sort=taggerdate --format '%(tag)' refs/tags | tail -n1 | cut -d- -f1`
ACT_METADATA=`git for-each-ref --sort=taggerdate --format '%(tag)' refs/tags | tail -n1 | cut -d- -f2`
#VERSION_METADATA="rc1"

#echo $ACT_VERSION
#echo $VERSION_TYPE

if [ ! "$VERSION" = "1.0.0" ]
then
  if [ ! -z $VERSION_METADATA ]
  then
    VERSION="$VERSION-$VERSION_METADATA"
  fi
  echo $VERSION
  exit 0
fi

if [ ! -z $ACT_VERSION ]
then

  if [ "$VERSION_TYPE" = "patch" ]
  then
    VERSION=$(echo $ACT_VERSION | ( IFS=".$IFS" ; read a b c && echo $a.$b.$((c + 1)) ))
  fi

  if [ "$VERSION_TYPE" = "minor" ]
  then
    VERSION=$(echo $ACT_VERSION | ( IFS=".$IFS" ; read a b c && echo $a.$((b + 1)).0))
  fi

  if [ "$VERSION_TYPE" = "major" ]
  then
    VERSION=$(echo $ACT_VERSION | ( IFS=".$IFS" ; read a b c && echo $((a + 1)).0.0 ))
  fi

  #echo $VERSION

  if [ ! -z $VERSION_METADATA ]
  then
    VERSION="$VERSION-$VERSION_METADATA"
    #echo $VERSION
  fi

  if [ $ACT_METADATA = "staging" ]
  then
    VERSION=$ACT_VERSION
  fi
fi

echo $VERSION
