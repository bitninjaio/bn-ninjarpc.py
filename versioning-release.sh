#!/bin/sh
REPO_NAME=$1

VERSION=$(echo $REPO_NAME | cut -d'/' -f2)
echo "${VERSION}-staging"
